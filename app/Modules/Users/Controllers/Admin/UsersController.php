<?php
namespace App\Modules\Users\Controllers\Admin;

use HZ\Illuminate\Mongez\Http\RestfulApiController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class UsersController extends RestfulApiController
{
    /**
     * Controller info
     *
     * @var array
     */
    protected $controllerInfo = [
        'repository' => 'users',
        'listOptions' => [
            'select' => [],
            'paginate' => null, // inherit by default
        ],
        'rules' => [
            'all' => [
                'name' => ['string', 'max:255'],
                'email' => ['email', 'max:255', 'unique:users,email'],
                'password' => ['string', 'regex:/^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[&-_.!@#$%*?+~])/'],
                'image' => ['image', 'mimes:jpeg,jpg,png', 'max:5000'],
                'mobile' => ['regex:/^(\+\d{1,3}[- ]?)?\d{10}$/', 'unique:users,mobile'],
                'gender' => ['in:male,female', 'string'],
            ],
            'store' => [
                'name' => ['required'],
                'email' => ['required'],
                'password' => ['required'],
                'mobile' => ['required'],
                'gender' => ['required'],
            ],
            'update' => [
                'name' => ['required', 'sometimes'],
                'email' => ['required', 'sometimes'],
                'password' => ['required', 'sometimes'],
                'mobile' => ['required', 'sometimes'],
                'gender' => ['required', 'sometimes'],
            ],
            'patch' => [
                'name' => ['required', 'sometimes'],
                'email' => ['required', 'sometimes'],
                'password' => ['required', 'sometimes'],
                'mobile' => ['required', 'sometimes'],
                'gender' => ['required', 'sometimes'],
            ],
        ],
    ];

    public function beforeStoring(Request $request)
    {
        $validator = Validator::make($request->all(), [
                'email' => Rule::unique('users', 'email'),
                'mobile' => Rule::unique('users', 'mobile'),
                ]);
        return $validator->fails() ? $validator->errors() : [];
    }

    public function beforeUpdating($model, Request $request)
    {
        $validator = Validator::make($request->only(['email','mobile']), [
            'email' => Rule::unique('users', 'email')->whereNot('id', $model->id),
            'mobile' => Rule::unique('users', 'mobile')->whereNot('id', $model->id),
        ]);
        return $validator->fails() ? $validator->errors() : [];
    }

    public function beforePatching($model, Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => Rule::unique('users', 'email')->ignore($model->id),
            'mobile' => Rule::unique('users', 'mobile')->ignore($model->id),
        ]);
        return $validator->fails() ? $validator->errors() : [];
    }
}
