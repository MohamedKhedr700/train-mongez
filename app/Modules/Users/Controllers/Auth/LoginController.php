<?php

namespace App\Modules\Users\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Modules\Users\Requests\UserLoginRequest;
use HZ\Illuminate\Mongez\Http\RestfulApiController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\MessageBag;

class LoginController extends RestfulApiController
{
    /**
     * Controller info
     *
     * @var array
     */
    protected $controllerInfo = [
        'repository' => 'users',
        'listOptions' => [
            'select' =>[],
            'paginate' => null,
        ],
        'rules' => [
            'all' => [],
            'store' => [],
            'update' => [],
            'patch' => [],
        ],
    ];
    public function login(UserLoginRequest $request)
    {
        $data = $request->only([ 'email', 'password']);
        $user = $this->repository->getByModel('email', $data['email']);
        $checkPassword = Hash::check($data['password'], $user->password);
        return $user && $checkPassword ? $this->success(['accessToken' => $user->createToken($user->email)->plainTextToken]) : $this->badRequest((new MessageBag())->add('email', trans('auth.failed')));
    }
}
