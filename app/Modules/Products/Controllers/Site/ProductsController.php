<?php
namespace App\Modules\Products\Controllers\Site;

use Illuminate\Http\Request;
use HZ\Illuminate\Mongez\Http\ApiController;

class ProductsController extends ApiController
{
    /**
     * Repository name
     *
     * @var string
     */
    public const REPOSITORY_NAME = 'products';

    /**
     * {@inheritDoc}
     */
    public function index(Request $request)
    {
        $options = [
            'tagIds' =>  $request->input('tagIds') ? collect($request->input('tagIds'))->map(function ($item) {
                return (int) $item;
            })->toArray() : null,
            'brandId' => $request->input('brandId') ? (int) $request->input('brandId') : null,
            'description' => $request->input('description'),
            'title' => $request->input('title'),
            'minPrice' => $request->input('minPrice'),
            'maxPrice' => $request->input('maxPrice'),
            'count' => $request->input('count'),
            'orderBy' => (array) $request->input('orderBy'),
            'tagName' => $request->input('tagName'),
            'brandName' => $request->input('brandName'),
        ];

        return $this->success([
            'records' => $this->repository->list($options),
        ]);
    }

    /**
     * {@inheritDoc}
     */
    public function show($id, Request $request)
    {
        $record = $this->repository->getPublished($id);

        if (! $record) {
            return $this->notFound('notFoundRecord');
        }

        return $this->success([
            'record' => $record,
        ]);
    }
}
