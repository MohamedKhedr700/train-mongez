<?php
namespace App\Modules\Products\Controllers\Admin;

use App\Modules\Brands\Repositories\BrandsRepository;
use App\Modules\Tags\Repositories\TagsRepository;
use HZ\Illuminate\Mongez\Events\Events;
use HZ\Illuminate\Mongez\Http\RestfulApiController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\MessageBag;
use Illuminate\Validation\Rule;

class ProductsController extends RestfulApiController
{
    /**
     * Controller info
     *
     * @var array
     */
    protected $controllerInfo = [
        'repository' => 'products',
        'listOptions' => [
            'select' => [],
            'paginate' => null, // inherit by default
        ],
        'rules' => [
            'all' => [
                'title' => ['json'],
                'description' => ['string'],
                'count' => ['integer'],
                'image' => ['image', 'mimes:jpeg,jpg,png', 'max:5000'],
                'multiImages' => ['array'],
                'multiImages.*' => ['image', 'mimes:jpeg,jpg,png', 'max:5000'],
                'tags' => ['array'],
                'slug' => ['string'],
            ],
            'store' => [
                'title' => ['required'],
                'description' => ['required'],
                'count' => ['required'],
                'image' => ['required'],
                'brand' => ['required'],
                'tags' => ['required'],
                'slug' => ['required'],
            ],
            'update' => [],
            'patch' => [],
        ],
    ];

    public function beforeStoring(Request $request)
    {
        $validationData = collect($request->only(['brand', 'tags']))->map(function ($item) {
            return is_array($item) ? collect($item)->map(function ($item) {
                return (int) $item;
            })->toArray() : (int) $item;
        })->toArray();
        $validator = Validator::make($validationData, [
            'brand' => Rule::exists('brands', 'id'),
            'tags.*' => Rule::exists('tags', 'id'),
        ]);
        return $validator->fails() ? $validator->errors() : [];
    }
}
