<?php

use Illuminate\Support\Facades\Route;
use App\Modules\Products\Controllers\Site\ProductsController;

/*
|--------------------------------------------------------------------------
| Products Site Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your main "front office" application.
| Please note that this file is auto imported in the main routes file, so it will inherit the main "prefix"
| and "namespace", so don't edit it to add for example "api" as a prefix.
*/

Route::group([
    'prefix' => 'products',
], function () {
    // list records
    Route::get('/', [ProductsController::class, 'index']);
    // one record
    Route::get('/{id}', [ProductsController::class, 'show']);
});
