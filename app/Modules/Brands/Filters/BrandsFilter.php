<?php
namespace App\Modules\Brands\Filters;

use HZ\Illuminate\Mongez\Database\Filters\MongoDBFilter;

class BrandsFilter extends MongoDBFilter
{
    /**
     * List with all filter.
     *
     * filterName => functionName
     * @const array
     */
    const FILTER_MAP = [];
}
