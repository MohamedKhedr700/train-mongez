<?php
namespace App\Modules\Brands\Controllers\Admin;

use HZ\Illuminate\Mongez\Http\RestfulApiController;

class BrandsController extends RestfulApiController
{
    /**
     * Controller info
     *
     * @var array
     */
    protected $controllerInfo = [
        'repository' => 'brands',
        'listOptions' => [
            'select' => [],
            'paginate' => null, // inherit by default
        ],
        'rules' => [
            'all' => [
                'title' => ['json'],
                'description' => ['string'],
                'image' => ['image'],
                'slug' => ['string'],
            ],
            'store' => [
                'description' => ['required'],
                'slug' => ['required'],
//                'image' => ['required'],
            ],
            'update' => [
                'title' => ['sometimes', 'required'],
                'description' => ['sometimes', 'required'],
                'image' => ['sometimes', 'required'],
            ],
            'patch' => [
                'title' => ['sometimes', 'required'],
                'description' => ['sometimes', 'required'],
                'image' => ['sometimes', 'required'],
            ],
        ],
    ];



//    protected function beforeStoring(Request $request)
//    {
//        return is_json($request->input('title')) ? null : (new \Illuminate\Support\MessageBag)->add('title','required as json');
//    }
}
