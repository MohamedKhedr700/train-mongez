<?php
namespace App\Modules\Brands\Controllers\Site;

use Illuminate\Http\Request;
use HZ\Illuminate\Mongez\Http\ApiController;

class BrandsController extends ApiController
{
    /**
     * Repository name
     *
     * @var string
     */
    public const REPOSITORY_NAME = 'brands';

    /**
     * {@inheritDoc}
     */
    public function index(Request $request)
    {
        $options = [
            'title' => $request->get('title'),
            'description' => $request->get('description'),
            'select' =>  $request->get('select'),
            'sortBy' =>  $request->get('sortBy'),
        ];

        return $this->success([
            'records' => $this->repository->listPublished($options),
        ]);
    }

    /**
     * {@inheritDoc}
     */
    public function show($id, Request $request)
    {
        $record = $this->repository->getPublished($id);

        if (! $record) {
            return $this->notFound('notFoundRecord');
        }

        return $this->success([
            'record' => $record,
        ]);
    }
}
