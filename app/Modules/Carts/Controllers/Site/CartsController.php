<?php
namespace App\Modules\Carts\Controllers\Site;

use Illuminate\Http\Request;
use HZ\Illuminate\Mongez\Http\ApiController;
use Illuminate\Support\Facades\Validator;

class CartsController extends ApiController
{
    /**
     * Repository name
     *
     * @var string
     */
    public const REPOSITORY_NAME = 'carts';

    /**
     * {@inheritDoc}
     */
    public function index(Request $request)
    {
        $options = [
            'userId' => user()->id,
            'orderBy' => (array) $request->input('orderBy'),

        ];

        return $this->success([
            'records' => $this->repository->list($options),
        ]);
    }

    /**
     * {@inheritDoc}
     */

    public function store(Request $request)
    {
        $validationData = collect($request->only(['product', 'quantity']))->map(function ($item) {
             return (int) $item;
        })->toArray();
        $validator = Validator::make($validationData, [
            'product' => ['required', 'exists:products,id'],
            'quantity' => ['required', 'integer'],
        ]);
        if ($validator->fails()) {
            return $this->badRequest($validator->errors());
        }
        return $this->repository->create($validationData) ? $this->successCreate(['record' => $this->repository->wrap($this->repository->getByModel('userId', user()->id))]) : $this->badRequest(['error' => 'Cannot add to cart']);
    }

    public function delete($id)
    {
        $cart = $this->repository->getByModel('userId', user()->id);
        if (is_null($product = collect($cart->products)->where('id', $id)->first())) {
            return $this->notFound();
        }
        return  $cart->disassociate($product, 'products')->save() ? $this->success(['record' => $this->repository->wrap($cart)]) : $this->badRequest(['error' => 'Cannot delete from cart']);
    }
}
