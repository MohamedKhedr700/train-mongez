<?php
namespace App\Modules\Carts\Providers;

use HZ\Illuminate\Mongez\Providers\ModuleServiceProvider;

class CartsServiceProvider extends ModuleServiceProvider
{
    /**
     * List of routes files
     *
     * @const array
     */
    const ROUTES_TYPES = ['site'];

    /**
     * Module build type
     * Possible values: api|ui
     *
     * @const strong
     */
    const BUILD_MODE = 'api';


    /**
     * Translation Name Prefix
     *
     * @const string
     */
    public const TRANSLATION_PREFIX = 'carts';
}
