<?php
namespace App\Modules\Carts\Filters;

use HZ\Illuminate\Mongez\Database\Filters\MongoDBFilter;

class CartsFilter extends MongoDBFilter
{
    /**
     * List with all filter.
     *
     * filterName => functionName
     * @const array
     */
    const FILTER_MAP = [];
}
