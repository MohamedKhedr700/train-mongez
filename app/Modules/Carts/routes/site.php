<?php

use Illuminate\Support\Facades\Route;
use App\Modules\Carts\Controllers\Site\CartsController;

/*
|--------------------------------------------------------------------------
| Carts Site Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your main "front office" application.
| Please note that this file is auto imported in the main routes file, so it will inherit the main "prefix"
| and "namespace", so don't edit it to add for example "api" as a prefix.
*/

Route::group([
    'prefix' => 'carts', 'middleware' => 'auth:api'
], function () {
    // list records
    Route::get('/', [CartsController::class, 'index']);
    // one record
    Route::get('/{id}', [CartsController::class, 'show']);
    // add product to cart
    Route::post('/', [CartsController::class, 'store']);
    // delete product from cart
    Route::delete('{id}', [CartsController::class, 'delete']);
});
