<?php
namespace App\Modules\Tags\Controllers\Admin;

use HZ\Illuminate\Mongez\Http\RestfulApiController;

class TagsController extends RestfulApiController
{
    /**
     * Controller info
     *
     * @var array
     */
    protected $controllerInfo = [
        'repository' => 'tags',
        'listOptions' => [
            'select' => [],
            'paginate' => null, // inherit by default
        ],
        'rules' => [
            'all' => [
                'title' => ['json'],
                'slug' => ['string'],
                'price' => ['numeric'],
            ],
            'store' => [
                'title' => ['required'],
                'slug' => ['required'],
                'price' => ['required'],
            ],
            'update' => [
                'title' => ['sometimes', 'required'],
                'slug' => ['sometimes', 'required'],
                'price' => ['sometimes', 'required'],
            ],
            'patch' => [
                'title' => ['sometimes', 'required'],
                'slug' => ['sometimes', 'required'],
                'price' => ['sometimes', 'required'],
            ],
        ],
    ];
}
