<?php

use Illuminate\Support\Facades\Route;
use App\Modules\Tags\Controllers\Site\TagsController;

/*
|--------------------------------------------------------------------------
| Tags Site Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your main "front office" application.
| Please note that this file is auto imported in the main routes file, so it will inherit the main "prefix"
| and "namespace", so don't edit it to add for example "api" as a prefix.
*/

Route::group([
    'prefix' => 'tags',
], function () {
    // list records
    Route::get('/', [TagsController::class, 'index']);
    // one record
    Route::get('/{id}', [TagsController::class, 'show']);
});
