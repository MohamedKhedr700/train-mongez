<?php

namespace App\Modules\Tags\Utils;

use App\Modules\Tags\Providers\TagsServiceProvider;

class TagsUtils
{
    /**
     * Translate the given message.
     *
     * @param  string|null  $key
     * @param  array  $replace
     * @param  string|null  $locale
     * @return \Illuminate\Contracts\Translation\Translator|string|array|null
     */
    public static function trans($key = null, $replace = [], $locale = null)
    {
        static $baseTranslationName;

        if (! $baseTranslationName) {
            $baseTranslationName = TagsServiceProvider::TRANSLATION_PREFIX;
        }

        return trans($baseTranslationName . '::' . $key, $replace, $locale);
    }
}
