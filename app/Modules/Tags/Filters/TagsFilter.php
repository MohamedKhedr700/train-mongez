<?php
namespace App\Modules\Tags\Filters;

use HZ\Illuminate\Mongez\Database\Filters\MongoDBFilter;

class TagsFilter extends MongoDBFilter
{
    /**
     * List with all filter.
     *
     * filterName => functionName
     * @const array
     */
    const FILTER_MAP = [];
}
