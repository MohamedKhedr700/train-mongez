<?php
namespace App\Modules\Admins\Controllers\Admin;

use App\Modules\Users\Traits\Auth\AccessToken;
use HZ\Illuminate\Mongez\Http\RestfulApiController;
use Laravel\Sanctum\HasApiTokens;

class AdminsController extends RestfulApiController
{
    /**
     * Controller info
     *
     * @var array
     */
    protected $controllerInfo = [
        'repository' => 'admins',
        'listOptions' => [
            'select' => [],
            'paginate' => null, // inherit by default
        ],
        'rules' => [
            'all' => [],
            'store' => [],
            'update' => [],
            'patch' => [],
        ],
    ];
}
