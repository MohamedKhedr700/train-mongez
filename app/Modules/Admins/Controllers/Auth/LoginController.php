<?php

namespace App\Modules\Admins\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Modules\Admins\Requests\AdminLoginRequest;
use HZ\Illuminate\Mongez\Http\RestfulApiController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\MessageBag;

class LoginController extends RestfulApiController
{
    /**
     * Controller info
     *
     * @var array
     */
    protected $controllerInfo = [
        'repository' => 'admins',
        'listOptions' => [
            'select' =>[],
            'paginate' => null,
        ],
        'rules' => [
            'all' => [],
            'store' => [],
            'update' => [],
            'patch' => [],
        ],
    ];
    public function login(AdminLoginRequest $request)
    {

        $data = $request->only([ 'email', 'password']);
        $admin = $this->repository->getByModel('email', $data['email']);
        $checkPassword = Hash::check($data['password'], $admin->password);
        return $admin && $checkPassword ? $this->success(['accessToken' => $admin->createToken($admin->email)->plainTextToken]) : $this->badRequest((new MessageBag())->add('email', trans('auth.failed')));
    }
}
