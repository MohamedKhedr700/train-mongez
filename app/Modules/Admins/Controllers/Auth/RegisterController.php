<?php

namespace App\Modules\Admins\Controllers\Auth;

use App\Http\Controllers\Controller;
use HZ\Illuminate\Mongez\Http\RestfulApiController;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class RegisterController extends RestfulApiController
{
    /**
     * Controller info
     *
     * @var array
     */
    protected $controllerInfo = [
        'repository' => 'admins',
        'listOptions' => [
            'select' =>[],
            'paginate' => null,
        ],
        'rules' => [
            'all' => [],
            'store' => [
                'email' => ['required', 'unique:admins,email', 'email', 'max:255'],
                'name' => ['required', 'string', 'max:255'],
                'password' => ['required', 'string', 'regex:/^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[&-_.!@#$%*?+~])/'],
                'image' => ['image', 'mimes:jpeg,jpg,png', 'max:5000'],
            ],
            'update' => [],
            'patch' => [],
        ],
    ];

    public function register(Request $request)
    {
        $data = $request->only(['name', 'email', 'password', 'image']);
        $adminResponse = $this->store($request);
        switch (true) {
            case $adminResponse->status() == Response::HTTP_BAD_REQUEST:
                return $adminResponse;
            case $adminResponse->status() == Response::HTTP_CREATED:
                $admin = $this->repository->getByModel('email', $data['email']);
                return $this->successCreate(['accessToken' => $admin->createToken($admin->email)->plainTextToken]);
        }
    }
}
