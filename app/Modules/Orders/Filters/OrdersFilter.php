<?php
namespace App\Modules\Orders\Filters;

use HZ\Illuminate\Mongez\Database\Filters\MongoDBFilter;

class OrdersFilter extends MongoDBFilter
{
    /**
     * List with all filter.
     *
     * filterName => functionName
     * @const array
     */
    const FILTER_MAP = [];
}
