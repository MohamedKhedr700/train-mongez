<?php

return [
    /*
    |--------------------------------------------------------------------------
    | Orders Translation File
    |--------------------------------------------------------------------------
    | You can access this file translation by trans('orders::order.key')
    | Or you can use the OrdersUtils::trans('key')
    | Or you can use the OrdersUtils::trans('order.key')
    |
    */
    //
    'empty-cart' => 'عربة التسوق الخاصة بك فارغة ، الرجاء إضافة بعض المنتجات',
];
