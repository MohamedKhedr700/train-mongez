<?php
namespace App\Modules\Orders\Controllers\Admin;

use App\Modules\Orders\Utils\OrdersUtils;
use HZ\Illuminate\Mongez\Http\RestfulApiController;
use Illuminate\Http\Request;
use Illuminate\Support\MessageBag;

class OrdersController extends RestfulApiController
{
    /**
     * Controller info
     *
     * @var array
     */
    protected $controllerInfo = [
        'repository' => 'orders',
        'listOptions' => [
            'select' => [],
            'paginate' => null, // inherit by default
        ],
        'rules' => [
            'all' => [],
            'store' => [],
            'update' => [],
            'patch' => [],
        ],
    ];

    /**
     * @throws \HZ\Illuminate\Mongez\Repository\NotFoundRepositoryException
     */
    public function beforeStoring(Request $request)
    {
        if ($userCart = repo('carts')->getByModel('userId', user()->id)) {
            return count($userCart->products) ? null : (new MessageBag())->add('cart', OrdersUtils::trans('order.empty-cart'));
        } else {
            return (new MessageBag())->add('cart', OrdersUtils::trans('order.empty-cart'));
        }
    }
}
