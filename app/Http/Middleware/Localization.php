<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class Localization
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next)
    {
        $local = $request->hasHeader('locale-code') && in_array(strtolower($request->header('locale-code')), config('mongez.localeCodes')) ?
            strtolower($request->header('locale-code')) :
            app()->getLocale();
        app()->setLocale($local);

        return $next($request);
    }
}
