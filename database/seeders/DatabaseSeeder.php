<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
//			\App\Modules\Brands\Database\Seeders\BrandSeeder::class,
//			\App\Modules\Products\Database\Seeders\ProductSeeder::class,
//			\App\Modules\Tags\Database\Seeders\TagSeeder::class,
			\App\Modules\Admins\Database\Seeders\AdminSeeder::class,
			\App\Modules\Users\Database\Seeders\UserSeeder::class,
			\App\Modules\Carts\Database\Seeders\CartSeeder::class,
			\App\Modules\Orders\Database\Seeders\OrderSeeder::class,
			// DatabaseSeeds: DO NOT Remove This Line.
        ]);
        // \App\Models\User::factory(10)->create();
    }
}
